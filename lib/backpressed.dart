library backpressed_android;

import 'package:flutter/widgets.dart';

import 'backpressed_controller.dart';

/// For handling the back button in android. This widget must be used at the
/// highest position in the widget tree to enable the back button feature for
/// its descendants
class BackPressedWidget extends InheritedWidget {
  BackPressedWidget({Key key, Widget child})
      : super(key: key, child: BackPressedControllerWidget(child: child));

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return false;
  }

  static BackPressedWidget of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(BackPressedWidget)
    as BackPressedWidget;
  }

  void listen(void onBackPressed()) {
    (child as BackPressedControllerWidget).listen(onBackPressed);
  }

  void stopListening() {
    (child as BackPressedControllerWidget).stopListening();
  }
}

// ignore: must_be_immutable
class BackPressedControllerWidget extends StatelessWidget {
  final Widget child;
  BackPressedController _backPressedHandler;
  bool _exitApp = true;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: child, onWillPop: _onWillPop);
  }

  BackPressedControllerWidget({Key key, @required this.child})
      : assert(child != null),
        super(key: key);

  void listen(void onBackPressed()) {
    _exitApp = false;
    _backPressedHandler = BackPressedController();
    _backPressedHandler.listen(() {
      onBackPressed();
      _exitApp = true;
    });
  }

  void stopListening() {
    _backPressedHandler.dispose();
    this._exitApp = true;
  }

  Future<bool> _onWillPop() {
    if (_exitApp) {
      return Future.value(true);
    } else {
      _backPressedHandler.backPressedTriggered();
      return Future.value(false);
    }
  }
}

