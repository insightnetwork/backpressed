import 'dart:async';

class BackPressedController {
  StreamSubscription _subscription;
  StreamController _backPressedController = StreamController.broadcast();

  void backPressedTriggered() {
    _backPressedController.sink.add(true);
  }

  void listen(void onBackPressed()) {
    _subscription = _backPressedController.stream.listen((n) {
      onBackPressed();
    });
  }

  void dispose() {
    _subscription.cancel();
    _subscription = null;
  }
}
