# backpressed_android

Backpressed widget for handling the back button in android

## Getting Started

1- Choose the highest widget on the tree below which you want to enable the back button and wrap it in
`BackPressedWidget`.

2- Go to your widget of interest which you want it to reflect on the back button press and write
this after `super.didChangeDependencies();` by overriding the `didChangeDependencies()` method:

 ```BackPressedWidget.of(context).listen(() {
    print('do whatever you want to be done when back button is pressed');
})```

